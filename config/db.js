const Mongoose  = require('mongoose')

require('dotenv').config()

const host = process.env.ENVIRONMENT.toLowerCase() === 'testing' ? process.env.DB_HOST_TESTING : process.env.DB_HOST_DEV

Mongoose.connect(host, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useFindAndModify: false,
  useCreateIndex: true
}, () => {
  console.log('Connected to DB')
})