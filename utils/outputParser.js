require('dotenv').config()

const success = (res, code, data = null,  msg = null) => {
  const json = {
    code: code,
    status: true
  }

  if (msg) json['message'] = msg
  if (data) json['result'] = data
  res.status(code).json(json)
}

const fail = (res, code, err, errServer, msg = null) => {
  const json = {
    code: code,
    status: false,
    error: err
  }

  if (process.env.ENVIRONMENT.toLowerCase() === 'development') {
    json['error_stack'] = errServer ? errServer.stack : null;
  }

  if (msg) json['message'] = msg

  res.status(code).json(json)
}

module.exports = {
  success,
  fail
}