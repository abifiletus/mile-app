const { checkSchema, check } = require('express-validator')

const errorFormatter = ({ msg }) => {
  return msg
}

const _validate_email = (email) => {
  const regex = /(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/
  return !!email.match(regex);
}

const _origin_destination = async (value) => {
  if (!value.hasOwnProperty('customer_name')) throw new Error('Customer name is required.')
  if (!value.customer_name) throw new Error('Customer name is required.')
  value.customer_name = value.customer_name.trim()

  if (!value.hasOwnProperty('customer_address')) throw new Error('Customer address is required.')
  if (!value.customer_address) throw new Error('Customer address is required.')
  value.customer_address = value.customer_address.trim()

  if (!value.hasOwnProperty('customer_email')) value['customer_email'] = ''
  if (!value.customer_email) value['customer_email'] = ''
  if (value.customer_email) {
    if (!await _validate_email(value.customer_email)) throw new Error('Customer email must a valid email.')
  }
  value.customer_email = value.customer_email.trim()

  if (!value.hasOwnProperty('customer_phone')) throw new Error('Customer phone is required.')
  if (!value.customer_phone) throw new Error('Customer phone is required.')
  value.customer_phone = value.customer_phone.trim()

  if (!value.hasOwnProperty('customer_address_detail')) value['customer_address_detail'] = ''
  if (!value.customer_address_detail) value['customer_address_detail'] = ''
  value.customer_address_detail = value.customer_address_detail.trim()

  if (!value.hasOwnProperty('customer_zip_code')) throw new Error('Customer zip code is required.')
  if (!value.customer_zip_code) throw new Error('Customer zip code is required.')
  value.customer_zip_code = value.customer_zip_code.trim()

  if (!value.hasOwnProperty('zone_code')) throw new Error('Zone code is required.')
  if (!value.zone_code) throw new Error('Zone code is required.')
  value.zone_code = value.zone_code.trim()

  if (!value.hasOwnProperty('organization_id')) throw new Error('Organization id is required.')
  if (typeof value.organization_id !== "number") throw new Error('Organization id must a number.')
  value.organization_id = parseInt(value.organization_id)

  if (!value.hasOwnProperty('location_id')) throw new Error('Location id is required.')
  if (!value.location_id) throw new Error('Location id is required.')
  if (value.location_id.length != 24) throw new Error('Location id must have 24 characters.')
  value.location_id = value.location_id.trim()

  return value
}

const postValidation = checkSchema({
  transaction_id: {
    in: ['body', 'params', 'query'],
    notEmpty: {
      errorMessage: 'Transaction ID is required.'
    },
    isLength: {
      min: 36,
      max: 36,
      errorMessage: 'Transaction ID must 36 characters.'
    },
    escape: true,
    trim: true
  },
  customer_name: {
    in: ['body'],
    notEmpty: {
      errorMessage: 'Customer name is required.'
    },
    escape: true,
    trim: true
  },
  customer_code: {
    in: ['body'],
    notEmpty: {
      errorMessage: 'Customer code is required.'
    },
    errorMessage: 'Customer code must a number.',
    isInt: true,
    toInt: true
  },
  transaction_amount: {
    in: ['body'],
    notEmpty: {
      errorMessage: 'Transaction amount is required.'
    },
    errorMessage: 'Transaction amount must a number.',
    isInt: {
      min: 0
    },
    toInt: true
  },
  transaction_discount: {
    in: ['body'],
    toInt: true
  },
  transaction_additional_field: {
    in: ['body'],
    trim: true,
    escape: true
  },
  transaction_payment_type: {
    in: ['body'],
    notEmpty: {
      errorMessage: 'Transaction payment type is required.'
    },
    errorMessage: 'Transaction payment type must a number.',
    isInt: {
      min: 0
    },
    toInt: true
  },
  transaction_state: {
    in: ['body'],
    notEmpty: {
      errorMessage: 'Transaction state is required.'
    },
    escape: true,
    trim: true
  },
  transaction_code: {
    in: ['body'],
    notEmpty: {
      errorMessage: 'Transaction code is required.'
    },
    escape: true,
    trim: true
  },
  transaction_order: {
    in: ['body'],
    notEmpty: {
      errorMessage: 'Transaction order is required.'
    },
    errorMessage: 'Transaction order must a number.',
    isInt: {
      min: 0
    },
    toInt: true
  },
  location_id: {
    in: ['body'],
    notEmpty: {
      errorMessage: 'Location ID is required.'
    },
    isLength: {
      min: 24,
      max: 24,
      errorMessage: 'Location ID must 24 characters'
    },
    escape: true,
    trim: true
  },
  organization_id: {
    in: ['body'],
    notEmpty: {
      errorMessage: 'Organization ID is required.'
    },
    errorMessage: 'Organization ID must a number.',
    isInt: {
      min: 0
    },
    toInt: true
  },
  created_at: {
    in: ['body']
  },
  updated_at: {
    in: ['body']
  },
  transaction_payment_type_name: {
    in: ['body'],
    notEmpty: {
      errorMessage: 'Customer name is required.'
    },
    escape: true,
    trim: true
  },
  transaction_cash_amount: {
    in: ['body'],
    notEmpty: {
      errorMessage: 'Transaction cash amount is required.'
    },
    errorMessage: 'Transaction cash amount must a number.',
    isInt: {
      min: 0
    },
    toInt: true
  },
  transaction_cash_change: {
    in: ['body'],
    notEmpty: {
      errorMessage: 'Transaction cash change is required.'
    },
    errorMessage: 'Transaction cash change must a number.',
    isInt: {
      min: 0
    },
    toInt: true
  },
  customer_attribute: {
    custom: {
      options: async (value) => {
        if (!value) throw new Error('Customer attribute is required.')
        if (typeof value !== "object") throw new Error('Customer attribute must an object / array.')

        if (!value.hasOwnProperty('Nama_Sales')) throw new Error('Nama sales is required.')
        if (!value.Nama_Sales) throw new Error('Nama sales is required.')
        if (value.Nama_Sales.length < 3) throw new Error('Nama sales minimal length is 3 characters.')
        value.Nama_Sales = value.Nama_Sales.trim()

        if (!value.hasOwnProperty('TOP')) throw new Error('TOP is required.')
        if (!value.TOP) throw new Error('TOP is required.')
        value.TOP = value.TOP.trim()

        if (!value.hasOwnProperty('Jenis_Pelanggan')) throw new Error('Jenis pelanggan is required.')
        if (!value.Jenis_Pelanggan) throw new Error('Jenis pelanggan is required.')
        value.Jenis_Pelanggan = value.Jenis_Pelanggan.trim()

        return value
      }
    }
  },
  connote: {
    custom: {
      options: async (value) => {
        if (!value) throw new Error('Connote is required.')
        if (typeof value !== "object") throw new Error('Connote must an object / array.')

        if (!value.hasOwnProperty('connote_number')) throw new Error('Connote number is required.')
        if (typeof value.connote_number !== "number") throw new Error('Connote number must a number.')
        value.connote_number = parseInt(value.connote_number)

        if (!value.hasOwnProperty('connote_service')) throw new Error('Connote service is required.')
        if (!value.connote_service) throw new Error('Connote service is required.')
        value.connote_service = value.connote_service.trim()

        if (!value.hasOwnProperty('connote_service_price')) throw new Error('Connote service price is required.')
        if (typeof value.connote_service_price !== "number") throw new Error('Connote service price must a number.')
        value.connote_service_price = parseInt(value.connote_service_price)

        if (!value.hasOwnProperty('connote_amount')) throw new Error('Connote amount is required.')
        if (typeof value.connote_amount !== "number") throw new Error('Connote amount must a number.')
        value.connote_amount = parseInt(value.connote_amount)

        if (!value.hasOwnProperty('connote_code')) throw new Error('Connote code is required.')
        if (!value.connote_code) throw new Error('Connote code is required.')
        if (value.connote_code.length != 17) throw new Error('Connote code mush have 17 characters.')
        value.connote_code = value.connote_code.trim()

        if (!value.hasOwnProperty('connote_booking_code')) value['connote_booking_code'] = ''
        value.connote_booking_code = value.connote_booking_code.trim()

        if (!value.hasOwnProperty('connote_order')) throw new Error('Connote order is required.')
        if (typeof value.connote_order !== "number") throw new Error('Connote order must a number.')
        value.connote_order = parseInt(value.connote_order)

        if (!value.hasOwnProperty('connote_state')) throw new Error('Connote state code is required.')
        if (!value.connote_state) throw new Error('Connote state code is required.')
        value.connote_state = value.connote_state.trim()

        if (!value.hasOwnProperty('connote_state_id')) throw new Error('Connote state id is required.')
        if (typeof value.connote_state_id !== "number") throw new Error('Connote state id must a number.')
        value.connote_state_id = parseInt(value.connote_state_id)

        if (!value.hasOwnProperty('zone_code_from')) throw new Error('Zone code from is required.')
        if (!value.zone_code_from) throw new Error('Zone code from is required.')
        value.zone_code_from = value.zone_code_from.trim()

        if (!value.hasOwnProperty('zone_code_to')) throw new Error('Zone code to is required.')
        if (!value.zone_code_to) throw new Error('Zone code to is required.')
        value.zone_code_to = value.zone_code_to.trim()

        if (!value.hasOwnProperty('surcharge_amount')) value['surcharge_amount'] = 0
        if (!value.surcharge_amount) value.surcharge_amount = 0
        value.surcharge_amount = parseInt(value.surcharge_amount)

        if (!value.hasOwnProperty('transaction_id')) throw new Error('Transaction id is required.')
        if (!value.transaction_id) throw new Error('Transaction id is required.')
        if (value.transaction_id.length != 36) throw new Error('Transaction id mush have 36 characters.')
        value.transaction_id = value.transaction_id.trim()

        if (!value.hasOwnProperty('actual_weight')) throw new Error('Actual weight id is required.')
        if (typeof value.actual_weight !== "number") throw new Error('Actual weight id must a number.')
        value.actual_weight = parseInt(value.actual_weight)

        if (!value.hasOwnProperty('volume_weight')) value['volume_weight'] = 0
        if (!value.volume_weight) value.volume_weight = 0
        value.volume_weight = parseInt(value.volume_weight)

        if (!value.hasOwnProperty('chargeable_weight')) throw new Error('Chargeable weight id is required.')
        if (typeof value.chargeable_weight !== "number") throw new Error('Chargeable weight id must a number.')
        value.chargeable_weight = parseInt(value.chargeable_weight)

        if (!value.hasOwnProperty('organization_id')) throw new Error('Organization id id is required.')
        if (typeof value.organization_id !== "number") throw new Error('Organization id id must a number.')
        value.organization_id = parseInt(value.organization_id)

        if (!value.hasOwnProperty('location_id')) throw new Error('Location id is required.')
        if (!value.location_id) throw new Error('Location id is required.')
        if (value.location_id.length != 24) throw new Error('Location id mush have 24 characters.')
        value.location_id = value.location_id.trim()

        if (!value.hasOwnProperty('connote_total_package')) throw new Error('Connote total package is required.')
        if (typeof value.connote_total_package !== "number") throw new Error('Connote total package must a number.')
        value.connote_total_package = parseInt(value.connote_total_package)

        if (!value.hasOwnProperty('connote_surcharge_amount')) value['connote_surcharge_amount'] = 0
        if (!value.connote_surcharge_amount) value.connote_surcharge_amount = 0
        value.connote_surcharge_amount = parseInt(value.connote_surcharge_amount)

        if (!value.hasOwnProperty('connote_sla_day')) throw new Error('Connote sla day is required.')
        if (typeof value.connote_sla_day !== "number") throw new Error('Connote sla day must a number.')
        value.connote_sla_day = parseInt(value.connote_sla_day)

        if (!value.hasOwnProperty('location_name')) throw new Error('Location name is required.')
        if (!value.location_name) throw new Error('Location name is required.')
        value.location_name = value.location_name.trim()

        if (!value.hasOwnProperty('location_type')) throw new Error('Location type is required.')
        if (!value.location_type) throw new Error('Location type is required.')
        value.location_type = value.location_type.trim()

        if (!value.hasOwnProperty('source_tariff_db')) throw new Error('Source tariff db is required.')
        if (!value.source_tariff_db) throw new Error('Source tariff db is required.')
        value.source_tariff_db = value.source_tariff_db.trim()

        if (!value.hasOwnProperty('id_source_tariff')) throw new Error('ID source tariff is required.')
        if (typeof value.id_source_tariff !== "number") throw new Error('ID source tariff must a number.')
        value.id_source_tariff = parseInt(value.id_source_tariff)

        if (!value.hasOwnProperty('pod')) value['pod'] = ''
        if (value.pod === null) value['pod'] = ''
        value.pod = value.pod.trim()

        if (!value.hasOwnProperty('history')) value['history'] = []

        return value
      }
    }
  },
  connote_id: {
    custom: {
      options: async (value) => {
        if (!value) throw new Error('Connote id is required.')
        if (value.length != 36) throw new Error('Connote id must 36 characters.')
        return value.trim()
      }
    }
  },
  origin_data: {
    custom: {
      options: async (value) => {
        if (!value) throw new Error('Origin data is required.')
        if (typeof value !== "object") throw new Error('Origin data must an object / array.')

        return await _origin_destination(value)
      }
    }
  },
  destination_data: {
    custom: {
      options: async (value) => {
        if (!value) throw new Error('Destination data is required.')
        if (typeof value !== "object") throw new Error('Destination data must an object / array.')

        return await _origin_destination(value)
      }
    }
  },
  koli_data: {
    custom: {
      options: async (value) => {
        if (!value) throw new Error('Koli data is required.')
        if (typeof value !== "object") throw new Error('Koli data must an object / array.')

        await value.map(el => {
          if (!el.hasOwnProperty('koli_length')) el['koli_length'] = 0
          if (!value.koli_length) value.koli_length = 0
          el.koli_length = parseInt(el.koli_length)

          if (!el.hasOwnProperty('awb_url')) throw new Error('Awb url is required.')
          if (!el.awb_url) throw new Error('Awb url is required.')
          el.awb_url = el.awb_url.trim()

          if (!el.hasOwnProperty('koli_chargeable_weight')) throw new Error('Koli chargeable weight is required.')
          if (typeof el.koli_chargeable_weight !== "number") throw new Error('Koli chargeable weight must a number.')
          el.koli_chargeable_weight = parseInt(el.koli_chargeable_weight)

          if (!el.hasOwnProperty('koli_width')) el['koli_width'] = 0
          if (!value.koli_width) value.koli_width = 0
          el.koli_width = parseInt(el.koli_width)

          if (!el.hasOwnProperty('koli_surcharge')) el['koli_surcharge'] = []

          if (!el.hasOwnProperty('koli_height')) el['koli_height'] = 0
          if (!value.koli_height) value.koli_height = 0
          el.koli_height = parseInt(el.koli_height)

          if (!el.hasOwnProperty('koli_description')) throw new Error('Koli description is required.')
          if (!el.koli_description) throw new Error('Koli description is required.')
          el.koli_description = el.koli_description.trim()

          if (!el.hasOwnProperty('koli_formula_id')) el['koli_formula_id'] = ''
          if (!el.koli_formula_id) el['koli_formula_id'] = ''
          el.koli_formula_id = el.koli_formula_id.trim()

          if (!el.hasOwnProperty('connote_id')) throw new Error('Connote id is required.')
          if (!el.connote_id) throw new Error('Connote id is required.')
          if (el.connote_id.length != 36) throw new Error('Connote id must 36 characters.')
          el.connote_id = el.connote_id.trim()

          if (!el.hasOwnProperty('koli_volume')) el['koli_volume'] = 0
          if (!value.koli_volume) value.koli_volume = 0
          el.koli_volume = parseInt(el.koli_volume)

          if (!el.hasOwnProperty('koli_weight')) throw new Error('Koli weight is required.')
          if (typeof el.koli_weight !== "number") throw new Error('Koli weight must a number.')
          el.koli_weight = parseInt(el.koli_weight)

          if (!el.hasOwnProperty('koli_id')) throw new Error('Koli id is required.')
          if (!el.koli_id) throw new Error('Koli id is required.')
          if (el.koli_id.length != 36) throw new Error('Koli id must 36 characters.')
          el.koli_id = el.koli_id.trim()

          if (!el.hasOwnProperty('koli_custom_field')) throw new Error('Koli custom field is required.')
          if (!el.koli_custom_field) throw new Error('Koli custom field is required.')
          if (typeof el.koli_custom_field !== 'object') throw new Error('Koli custom field must an object / array.')

          if (!el.koli_custom_field.hasOwnProperty('awb_sicepat')) el.koli_custom_field['awb_sicepat'] = ''
          if (!el.koli_custom_field.awb_sicepat) el.koli_custom_field.awb_sicepat = ''
          el.koli_custom_field.awb_sicepat = el.koli_custom_field.awb_sicepat.trim()

          if (!el.koli_custom_field.hasOwnProperty('harga_barang')) el.koli_custom_field['harga_barang'] = ''
          if (!el.koli_custom_field.harga_barang) el.koli_custom_field.harga_barang = ''
          el.koli_custom_field.harga_barang = el.koli_custom_field.harga_barang.trim()

          if (!el.hasOwnProperty('koli_code')) throw new Error('Koli code is required.')
          if (!el.koli_code) throw new Error('Koli code is required.')
          if (el.koli_code.length != 19) throw new Error('Koli code must 19 characters.')
          el.koli_code = el.koli_code.trim()
        })

        return value
      }
    }
  },
  custom_field : {
    custom: {
      options: async (value) => {
        if (!value) throw new Error('Custom field is required.')
        if (typeof value !== "object") throw new Error('Custom field must an object / array.')

        if (!value.hasOwnProperty('catatan_tambahan')) value['catatan_tambahan'] = ''
        if (!value.catatan_tambahan) value['catatan_tambahan'] = ''
        value.catatan_tambahan = value.catatan_tambahan.trim()
      }
    }
  },
  currentLocation: {
    custom: {
      options: async (value) => {
        if (!value) throw new Error('Current location is required.')
        if (typeof value !== "object") throw new Error('Current location must an object / array.')

        if (!value.hasOwnProperty('name')) throw new Error('Name is required.')
        if (!value.name) throw new Error('Name is required.')
        value.name = value.name.trim()

        if (!value.hasOwnProperty('code')) throw new Error('Code is required.')
        if (!value.code) throw new Error('Code is required.')
        value.code = value.code.trim()

        if (!value.hasOwnProperty('type')) throw new Error('Type is required.')
        if (!value.type) throw new Error('Type is required.')
        value.type = value.type.trim()
      }
    }
  }
})

const deleteValidation = [
  check('transaction_id')
    .notEmpty()
    .withMessage('Transaction id is required.')
    .bail()
    .trim()
    .escape()
]

const _validateRequiredString = (value, key, message) => {
  let result

  if (!value) {
    result = {
      value: '',
      key: key,
      error: `${message} is required.`
    }
  } else {
    result = {
      value: value.trim(),
      key: key,
      error: ''
    }
  }

  return result
}

const _validateRequiredStringMaxMin = (value, key, message, min, max) => {
  let result

  if (!value) {
    result = {
      value: '',
      key: key,
      error: `${message} is required.`
    }
  } else if (value.length < min) {
    result = {
      value: '',
      key: key,
      error: `${message} must have minimal ${min} characters.`
    }
  } else if (value.length > max) {
    result = {
      value: '',
      key: key,
      error: `${message} must have maximal ${max} characters.`
    }
  } else {
    result = {
      value: value.trim(),
      key: key,
      error: ''
    }
  }

  return result
}

const _validateRequiredStringMin = (value, key, message, min) => {
  let result

  if (!value) {
    result = {
      value: '',
      key: key,
      error: `${message} is required.`
    }
  } else if (value.length < min) {
    result = {
      value: '',
      key: key,
      error: `${message} must have minimal ${min} characters.`
    }
  } else {
    result = {
      value: value.trim(),
      key: key,
      error: ''
    }
  }

  return result
}

const _validateRequiredNumber = (value, key, message) => {
  let result

  if (!value) {
    result = {
      value: '',
      key: key,
      error: `${message} is required.`
    }
  } else if(typeof value !== 'number') {
    result = {
      value: '',
      key: key,
      error: `${message} must a number.`
    }
  } else {
    result = {
      value: parseInt(value),
      key: key,
      error: ''
    }
  }

  return result
}

const _validateDefaultNumber = (value, key, defaultVal) => {
  let result

  if (!value) {
    result = {
      value: parseInt(defaultVal),
      key: key,
      error: ''
    }
  } else {
    result = {
      value: parseInt(value),
      key: key,
      error: ''
    }
  }

  return result
}

const _validateOnlyTrim = (value, key) => {
  return {
    value: value ? value.trim() : '',
    key: key,
    error: ''
  }
}

const _validateOnlyParseNumber = (value, key) => {
  return {
    value: value ? parseInt(value) : 0,
    key: key,
    error: ''
  }
}

const _validateOriginDestination = async (value, key) => {
  const res = []

  if (value.hasOwnProperty('customer_name')) {
    res.push(await _validateRequiredString(value.customer_name, 'customer_name', 'Customer name'))
  }

  if (value.hasOwnProperty('customer_address')) {
    res.push(await _validateRequiredString(value.customer_address, 'customer_address', 'Customer address'))
  }

  if (value.hasOwnProperty('customer_email')) {
    let check = await _validateRequiredString(value.customer_email, 'customer_email', 'Customer email')
    if (!check.error) {
      const checkEmail = await _validate_email(check.value)
      check = {
        value: check.value,
        key: check.key,
      }

      if (checkEmail) check['error'] = ''
      if (!checkEmail) check['error'] = 'Customer email must a valid email.'

      res.push(check)
    }
  }

  if (value.hasOwnProperty('customer_phone')) {
    res.push(await _validateRequiredString(value.customer_phone, 'customer_phone', 'Customer phone'))
  }

  if (value.hasOwnProperty('customer_address_detail')) {
    res.push(await _validateOnlyTrim(value.customer_address_detail, 'customer_address_detail'))
  }

  if (value.hasOwnProperty('customer_zip_code')) {
    res.push(await _validateRequiredString(value.customer_zip_code, 'customer_zip_code', 'Customer zip code'))
  }

  if (value.hasOwnProperty('zone_code')) {
    res.push(await _validateRequiredString(value.zone_code, 'zone_code', 'Zone code'))
  }

  if (value.hasOwnProperty('organization_id')) {
    res.push(await _validateRequiredNumber(value.organization_id, 'organization_id', 'Organization id'))
  }

  if (value.hasOwnProperty('location_id')) {
    res.push(await _validateRequiredStringMaxMin(value.location_id, 'location_id', 'Location id', 24, 24))
  }

  let hasError = false
  let result = {}
  let errors = {}
  await res.map(el => {
    if (el.error) {
      hasError = true
      errors = {
        value: '',
        key: [key ,el.key],
        error: el.error
      }
    } else {
      result[el.key] = el.value
    }
  })

  if (hasError) return errors

  return {
    value: result,
    key: 'customer_attribute',
    error: ''
  }
}

class patchValidation {

  async customer_name(value) {
    return await _validateRequiredString(value, 'customer_name', 'Customer name')
  }

  async customer_code(value) {
    return await _validateRequiredNumber(value, 'customer_code', 'Customer code')
  }

  async transaction_amount(value) {
    return await _validateRequiredNumber(value, 'transaction_amount', 'Transaction amount')
  }

  async transaction_discount(value) {
    return await _validateDefaultNumber(value, 'transaction_discount', 0)
  }

  async transaction_additional_field(value) {
    return await _validateOnlyTrim(value, 'transaction_additional_field')
  }

  async transaction_payment_type(value) {
    return await _validateRequiredString(value, 'transaction_payment_type', 'Transaction payment type')
  }

  async transaction_state(value) {
    return await _validateRequiredString(value, 'transaction_state', 'Transaction state')
  }

  async transaction_code(value) {
    return await _validateRequiredString(value, 'transaction_code', 'Transaction code')
  }

  async transaction_order(value) {
    return await _validateRequiredNumber(value, 'transaction_order', 'Transaction order')
  }

  async location_id(value) {
    return await _validateRequiredStringMaxMin(value, 'location_id', 'Location id', 24, 24)
  }

  async organization_id(value) {
    return await _validateRequiredNumber(value, 'organization_id', 'Organization id')
  }

  async created_at(value) {
    return {
      value: value,
      key: 'created_at',
      error: ''
    }
  }

  async updated_at(value) {
    return {
      value: value,
      key: 'updated_at',
      error: ''
    }
  }

  async transaction_payment_type_name(value) {
    return await _validateRequiredString(value, 'transaction_payment_type_name', 'Transaction payment type name')
  }

  async transaction_cash_amount(value) {
    return await _validateDefaultNumber(value, 'transaction_cash_amount', 0)
  }

  async transaction_cash_change(value) {
    return await _validateDefaultNumber(value, 'transaction_cash_change', 0)
  }

  async customer_attribute(value) {
    const res = []

    if (value.hasOwnProperty('Nama_Sales')) {
      res.push(await _validateRequiredStringMin(value.Nama_Sales, 'Nama_Sales', 'Nama sales', 3))
    }

    if (value.hasOwnProperty('TOP')) {
      res.push(await _validateRequiredString(value.TOP, 'TOP', 'TOP'))
    }

    if (value.hasOwnProperty('Jenis_Pelanggan')) {
      res.push(await _validateRequiredString(value.Jenis_Pelanggan, 'Jenis_Pelanggan', 'Jenis Pelanggan'))
    }

    let hasError = false
    let result = {}
    let errors = {}
    await res.map(el => {
      if (el.error) {
        hasError = true
        errors = {
          value: '',
          key: ['customer_attribute',el.key],
          error: el.error
        }
      } else {
        result[el.key] = el.value
      }
    })

    if (hasError) return errors

    return {
      value: result,
      key: 'customer_attribute',
      error: ''
    }
  }

  async connote(value) {
    const res = []

    if (value.hasOwnProperty('connote_id')) {
      res.push(await _validateRequiredStringMaxMin(value.connote_id, 'connote_id', 'Connote id', 36, 36))
    }

    if (value.hasOwnProperty('connote_number')) {
      res.push(await _validateRequiredNumber(value.connote_number, 'connote_number', 'Connote number'))
    }

    if (value.hasOwnProperty('connote_service')) {
      res.push(await _validateRequiredString(value.connote_service, 'connote_service', 'Connote service'))
    }

    if (value.hasOwnProperty('connote_service_price')) {
      res.push(await _validateRequiredNumber(value.connote_service_price, 'connote_service_price', 'Connote service price'))
    }

    if (value.hasOwnProperty('connote_amount')) {
      res.push(await _validateRequiredNumber(value.connote_amount, 'connote_amount', 'Connote amount'))
    }

    if (value.hasOwnProperty('connote_code')) {
      res.push(await _validateRequiredStringMaxMin(value.connote_code, 'connote_code', 'Connote code', 17, 17))
    }

    if (value.hasOwnProperty('connote_booking_code')) {
      res.push(await _validateOnlyTrim(value.connote_booking_code, 'connote_booking_code'))
    }

    if (value.hasOwnProperty('connote_order')) {
      res.push(await _validateRequiredNumber(value.connote_order, 'connote_order', 'Connote order'))
    }

    if (value.hasOwnProperty('connote_state')) {
      res.push(await _validateRequiredString(value.connote_state, 'connote_state', 'Connote state'))
    }

    if (value.hasOwnProperty('connote_state_id')) {
      res.push(await _validateRequiredNumber(value.connote_state_id, 'connote_state_id', 'Connote state id'))
    }

    if (value.hasOwnProperty('zone_code_from')) {
      res.push(await _validateRequiredString(value.zone_code_from, 'zone_code_from', 'Zone code from'))
    }

    if (value.hasOwnProperty('zone_code_to')) {
      res.push(await _validateRequiredString(value.zone_code_to, 'zone_code_to', 'Zone code to'))
    }

    if (value.hasOwnProperty('surcharge_amount')) {
      res.push(await _validateOnlyParseNumber(value, 'surcharge_amount'))
    }

    if (value.hasOwnProperty('transaction_id')) {
      res.push(await _validateRequiredStringMaxMin(value.transaction_id, 'transaction_id', 'Transaction id', 36, 36))
    }

    if (value.hasOwnProperty('actual_weight')) {
      res.push(await _validateRequiredNumber(value.actual_weight, 'actual_weight', 'Actual weight'))
    }

    if (value.hasOwnProperty('volume_weight')) {
      res.push(await _validateDefaultNumber(value, 'volume_weight', 0))
    }

    if (value.hasOwnProperty('chargeable_weight')) {
      res.push(await _validateRequiredNumber(value.chargeable_weight, 'chargeable_weight', 'Chargeable weight'))
    }

    if (value.hasOwnProperty('created_at')) {
      res.push({
        value: value.created_at,
        key: 'created_at',
        error: ''
      })
    }

    if (value.hasOwnProperty('updated_at')) {
      res.push({
        value: value.updated_at,
        key: 'updated_at',
        error: ''
      })
    }

    if (value.hasOwnProperty('organization_id')) {
      res.push(await _validateRequiredNumber(value.organization_id, 'organization_id', 'Organization id'))
    }

    if (value.hasOwnProperty('location_id')) {
      res.push(await _validateRequiredStringMaxMin(value.location_id, 'location_id', 'Location id', 24, 24))
    }

    if (value.hasOwnProperty('connote_total_package')) {
      res.push(await _validateRequiredNumber(value.connote_total_package, 'connote_total_package', 'Connote total package'))
    }

    if (value.hasOwnProperty('connote_surcharge_amount')) {
      res.push(await _validateDefaultNumber(value, 'connote_surcharge_amount', 0))
    }

    if (value.hasOwnProperty('connote_sla_day')) {
      res.push(await _validateRequiredNumber(value.connote_sla_day, 'connote_sla_day', 'Connote sla day'))
    }

    if (value.hasOwnProperty('location_name')) {
      res.push(await _validateRequiredString(value.location_name, 'location_name', 'Location name'))
    }

    if (value.hasOwnProperty('location_type')) {
      res.push(await _validateRequiredString(value.location_type, 'location_type', 'Location type'))
    }

    if (value.hasOwnProperty('source_tariff_db')) {
      res.push(await _validateRequiredString(value.source_tariff_db, 'source_tariff_db', 'Source tariff db'))
    }

    if (value.hasOwnProperty('id_source_tariff')) {
      res.push(await _validateRequiredNumber(value.id_source_tariff, 'id_source_tariff', 'Id source tariff'))
    }

    if (value.hasOwnProperty('pod')) {
      res.push(await _validateOnlyTrim(value.pod, 'pod'))
    }

    if (value.hasOwnProperty('history')) {
      res.push({
        value: value.history,
        key: 'history',
        error: ''
      })
    }

    let hasError = false
    let result = {}
    await res.map(el => {
      if (el.error) {
        hasError = true
        return {
          value: '',
          key: ['connote',el.key],
          error: el.error
        }
      } else {
        result[el.key] = el.value
      }
    })

    return {
      value: result,
      key: 'connote',
      error: ''
    }
  }

  async connote_id(value) {
    return await _validateRequiredStringMaxMin(value, 'connote_id', 'Connote id', 36, 36)
  }

  async origin_data(value) {
    return await _validateOriginDestination(value, 'origin_data')
  }

  async destination_data(value) {
    return await _validateOriginDestination(value, 'destination_data')
  }

  async koli_data(value) {
    const res = []

    for (let i=0; i<value.length; i++) {
      res[i] = []
      if (value[i].hasOwnProperty('koli_length')) {
        res[i].push(await _validateDefaultNumber(value[i].koli_length, 'koli_length', 0))
      }

      if (value[i].hasOwnProperty('awb_url')) {
        res[i].push(await _validateRequiredString(value[i].awb_url, 'awb_url', 'Awb url'))
      }

      if (value[i].hasOwnProperty('created_at')) {
        res[i].push({
          value: value[i].created_at,
          key: 'created_at',
          error: ''
        })
      }

      if (value[i].hasOwnProperty('koli_chargeable_weight')) {
        res[i].push(await _validateRequiredNumber(value[i].koli_chargeable_weight, 'koli_chargeable_weight', 'Koli chargeable weight'))
      }

      if (value[i].hasOwnProperty('koli_width')) {
        res[i].push(await _validateDefaultNumber(value[i].koli_width, 'koli_width', 0))
      }

      if (value[i].hasOwnProperty('koli_surcharge')) {
        res[i].push({
          value: value[i].koli_surcharge,
          key: 'koli_surcharge',
          error: ''
        })
      }

      if (value[i].hasOwnProperty('koli_height')) {
        res[i].push(await _validateDefaultNumber(value[i].koli_height, 'koli_height', 0))
      }

      if (value[i].hasOwnProperty('updated_at')) {
        res[i].push({
          value: value[i].updated_at,
          key: 'updated_at',
          error: ''
        })
      }

      if (value[i].hasOwnProperty('koli_description')) {
        res[i].push(await _validateRequiredString(value[i].koli_description, 'koli_description', 'Koli description'))
      }

      if (value[i].hasOwnProperty('koli_formula_id')) {
        res[i].push(await _validateOnlyTrim(value[i].koli_formula_id, 'koli_formula_id'))
      }

      if (value[i].hasOwnProperty('connote_id')) {
        res[i].push(await _validateRequiredStringMaxMin(value[i].connote_id, 'connote_id', 'Connote id', 36, 36))
      }

      if (value[i].hasOwnProperty('koli_volume')) {
        res[i].push(await _validateDefaultNumber(value[i].koli_volume, 'koli_volume', 0))
      }

      if (value[i].hasOwnProperty('koli_weight')) {
        res[i].push(await _validateRequiredNumber(value[i].koli_weight, 'koli_weight', 'Koli weight'))
      }

      if (value[i].hasOwnProperty('koli_id')) {
        res[i].push(await _validateRequiredStringMaxMin(value[i].koli_id, 'koli_id', 'Koli id', 36, 36))
      }

      if (value[i].hasOwnProperty('koli_custom_field')){
        const check = []
        let error = false
        let errorValidation = {}
        let resValidation = {}
        if (value[i].koli_custom_field.hasOwnProperty('awb_sicepat')) {
          check.push(await _validateOnlyTrim(value[i].koli_custom_field.awb_sicepat, 'awb_sicepat'))
        }

        if (value[i].koli_custom_field.hasOwnProperty('harga_barang')) {
          check.push(await _validateOnlyTrim(value[i].koli_custom_field.harga_barang, 'harga_barang'))
        }

        await check.map(el => {
          if (el.error) {
            error = true
            errorValidation = {
              value: '',
              key: 'koli_custom_field',
              error: el.error
            }
          } else {
            resValidation[el.key] = el.val
          }
        })

        res[i].push({
          value: resValidation,
          key: 'koli_custom_field',
          error: ''
        })
      }

      if (value[i].hasOwnProperty('koli_code')) {
        res[i].push(await _validateRequiredStringMaxMin(value[i].koli_code, 'koli_code', 'Koli code', 19, 19))
      }
    }

    let hasError = false
    let result = []
    let errors = {}

    await res.map(async (el,i) => {
      result[i] = {}

      await res[i].map(item => {
        if (item.error) {
          hasError = true
          errors = {
            value: '',
            key: ['koli_data',item.key],
            error: item.error
          }
        } else {
          result[i][item.key] = item.value
        }
      })
    })

    if (hasError) return errors

    return {
      value: result,
      key: 'koli_data',
      error: ''
    }
  }

  async custom_field(value) {
    let check = ''
    if (value.hasOwnProperty('catatan_tambahan')) {
      check = await _validateOnlyTrim(value.catatan_tambahan, 'catatan_tambahan')
      check = {
        value: {catatan_tambahan: check.value},
        key: check.key
      }
    }

    if (check) {
      return {
        value: check.value,
        key: 'custom_field',
        error: ''
      }
    }
  }

  async currentLocation(value) {
    const res = []

    if (value.hasOwnProperty('name')) {
      res.push(await _validateRequiredString(value.name, 'name', 'Name'))
    }

    if (value.hasOwnProperty('code')) {
      res.push(await _validateRequiredString(value.code, 'code', 'Code'))
    }

    if (value.hasOwnProperty('type')) {
      res.push(await _validateRequiredString(value.type, 'type', 'Type'))
    }

    let hasError = false
    let result = {}
    let errors = {}
    await res.map(el => {
      if (el.error) {
        hasError = true
        errors = {
          value: '',
          key: ['currentLocation',el.key],
          error: el.error
        }
      } else {
        result[el.key] = el.value
      }
    })

    if (hasError) return errors

    return {
      value: result,
      key: 'currentLocation',
      error: ''
    }
  }

}

module.exports = {
  errorFormatter,
  postValidation,
  deleteValidation,
  patchValidation: new patchValidation()
}