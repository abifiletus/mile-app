const express = require('express')
const router = express.Router()
const Package = require('../models/package')
const { matchedData, validationResult } = require('express-validator')
const { errorFormatter, postValidation, deleteValidation, patchValidation } = require('../utils/validation')
const outputParser = require('../utils/outputParser')

/**
 *  Get all data of package
 *  Return JSON of array data (check documentation 1. Get All Data)
 */
router.get('/', async (req, res, next) => {
  try {
    const packageData = await Package.find()
    return outputParser.success(res, 200, packageData, 'Successfully Get Data')
  } catch (err) {
    next(err)
  }
})

/**
 *  Get spesific data of package
 *  Retun JSON of single data (check documentation 2. Get Single Data)
 *
 *  params:
 *    - transaction_id : with string format
 */
router.get('/:transaction_id', deleteValidation, async (req, res, next) => {
  try {
    const errors = await validationResult(req).formatWith(errorFormatter)
    if (!errors.isEmpty()) {
      return outputParser.fail(res, 400, errors.mapped(), null, 'Validation Error')
    }
    const { transaction_id } = await matchedData(req)

    const packageData = await Package.findOne({ transaction_id })
    return outputParser.success(res, 200, packageData, 'Successfully Get Data')
  } catch (err) {
    next(err)
  }
})

/**
 *  Add new package data
 *  Return inserted package data (check documentation 3. Insert Data)
 *
 *  params:
 *    - transaction_id : with string format (required)
 *    - customer_name : with string format (required)
 *    - customer_code : with number format (required)
 *    - transaction_amount : with number format (required)
 *    - transaction_discount : with number format (default to 0)
 *    - transaction_additional_field : with string format
 *    - transaction_payment_type : with number format (required)
 *    - transaction_state : with string format (required)
 *    - transaction_code : with string format (required)
 *    - transaction_order : with number format (required)
 *    - location_id : with string format (required)
 *    - organization_id : with number format (required)
 *    - created_at: with date format,
 *    - updated_at: with date format,
 *    - transaction_payment_type_name : with string format (required)
 *    - transaction_cash_amount : with number format (required)
 *    - transaction_cash_change : with number format (required)
 *    - customer_attribute : with object format (required)
 *    - connote : with object format (required)
 *    - connote_id : with string format (required)
 *    - origin_data : with string format (required)
 *    - destination_data : with string format (required)
 *    - koli_data : with string format (required)
 *    - custom_field : with string format (required)
 *    - currentLocation : with string format (required)
 */
router.post('/', postValidation, async (req, res, next) => {
  try {
    const errors = await validationResult(req).formatWith(errorFormatter)
    if (!errors.isEmpty()) {
      return outputParser.fail(res, 400, errors.mapped(), null, 'Validation Error')
    }
    const {
      transaction_id,
      customer_name,
      customer_code,
      transaction_amount,
      transaction_discount,
      transaction_additional_field,
      transaction_payment_type,
      transaction_state,
      transaction_code,
      transaction_order,
      location_id,
      organization_id,
      created_at,
      updated_at,
      transaction_payment_type_name,
      transaction_cash_amount,
      transaction_cash_change,
      customer_attribute,
      connote,
      connote_id,
      origin_data,
      destination_data,
      koli_data,
      custom_field,
      currentLocation
    } = await matchedData(req)

    const packageData = new Package({
      transaction_id,
      customer_name,
      customer_code,
      transaction_amount,
      transaction_discount,
      transaction_additional_field,
      transaction_payment_type,
      transaction_state,
      transaction_code,
      transaction_order,
      location_id,
      organization_id,
      created_at,
      updated_at,
      transaction_payment_type_name,
      transaction_cash_amount,
      transaction_cash_change,
      customer_attribute,
      connote,
      connote_id,
      origin_data,
      destination_data,
      koli_data,
      custom_field,
      currentLocation
    })

    await packageData.save()
      .then(data => {
        return outputParser.success(res, 200, data, 'Successfully Inserting Data.')
      })
      .catch(err => {
        throw new Error(err)
      })
  } catch (err) {
    next(err)
  }
})

/**
 *  Update package data
 *  Return true / false (check documentation 4. Update Data)
 *
 *  Query params:
 *    - transaction_id : with string format (required)
 *
 *  params:
 *    - customer_name : with string format (required)
 *    - customer_code : with number format (required)
 *    - transaction_amount : with number format (required)
 *    - transaction_discount : with number format (default to 0)
 *    - transaction_additional_field : with string format
 *    - transaction_payment_type : with number format (required)
 *    - transaction_state : with string format (required)
 *    - transaction_code : with string format (required)
 *    - transaction_order : with number format (required)
 *    - location_id : with string format (required)
 *    - organization_id : with number format (required)
 *    - created_at: with date format,
 *    - updated_at: with date format,
 *    - transaction_payment_type_name : with string format (required)
 *    - transaction_cash_amount : with number format (required)
 *    - transaction_cash_change : with number format (required)
 *    - customer_attribute : with object format (required)
 *    - connote : with object format (required)
 *    - connote_id : with string format (required)
 *    - origin_data : with string format (required)
 *    - destination_data : with string format (required)
 *    - koli_data : with string format (required)
 *    - custom_field : with string format (required)
 *    - currentLocation : with string format (required)
 */
router.put('/:transaction_id', postValidation, async (req, res, next) => {
  try {
    const errors = await validationResult(req).formatWith(errorFormatter)
    if (!errors.isEmpty()) {
      return outputParser.fail(res, 400, errors.mapped(), null, 'Validation Error')
    }
    const {
      transaction_id,
      customer_name,
      customer_code,
      transaction_amount,
      transaction_discount,
      transaction_additional_field,
      transaction_payment_type,
      transaction_state,
      transaction_code,
      transaction_order,
      location_id,
      organization_id,
      created_at,
      updated_at,
      transaction_payment_type_name,
      transaction_cash_amount,
      transaction_cash_change,
      customer_attribute,
      connote,
      connote_id,
      origin_data,
      destination_data,
      koli_data,
      custom_field,
      currentLocation
    } = await matchedData(req)

    await Package.updateOne(
      { transaction_id },
      {
        $set: {
          customer_name,
          customer_code,
          transaction_amount,
          transaction_discount,
          transaction_additional_field,
          transaction_payment_type,
          transaction_state,
          transaction_code,
          transaction_order,
          location_id,
          organization_id,
          created_at,
          updated_at,
          transaction_payment_type_name,
          transaction_cash_amount,
          transaction_cash_change,
          customer_attribute,
          connote,
          connote_id,
          origin_data,
          destination_data,
          koli_data,
          custom_field,
          currentLocation
        }
      }
    )
      .then(() => {
        return outputParser.success(res, 200, true, 'Successfully edit data')
      })
      .catch(err => {
        throw new Error(err)
      })
  } catch (err) {
    next(err)
  }
})

/**
 *  Delete package data
 *  Return true / false (check documentation 5. Delete Data)
 *
 *  Query params:
 *    - transaction_id : with string format (required)
 */
router.patch('/:transaction_id', async (req, res, next) => {
  try {
    const body = req.body
    const transaction_id = req.params.transaction_id
    if (!body) return outputParser.fail(res, 400, 'Body must have a value.', '', 'Validation Error')
    const check = []
    const errors = {}
    const data = {}
    let hasError = false

    const bodyKeys = Object.keys(body)

    for (let i=0; i<bodyKeys.length; i++) {
      const checkRes = await patchValidation[bodyKeys[i]](body[bodyKeys[i]])
      check.push(checkRes)
    }

    await check.map(el => {
      if (el.error) {
        hasError = true
        if (typeof el.key === 'object') {
          if (!errors.hasOwnProperty(el.key[0])) errors[el.key[0]] = {}
          errors[el.key[0]][el.key[1]] = el.error
        } else {
          errors[el.key] = el.error
        }
      } else {
        data[el.key] = el.value
      }
    })

    if (hasError) {
      return outputParser.fail(res, 400, errors, '', 'Validation Error')
    } else {
      await Package.updateOne({ transaction_id }, { $set: data })
        .then(() => {
          return outputParser.success(res, 200, true, 'Successfully edit data')
        })
        .catch(err => {
          throw new Error(err)
        })
    }
  } catch (err) {
    next(err)
  }
})

/**
 *  Edit patch package data
 *  Return true / false (check documentation 6. Edit Patch Data)
 *
 *  Query params:
 *    - transaction_id : with string format (required)
 *
 *  Params: check in post data, you can change partially any data in post params
 */
router.delete('/:transaction_id', deleteValidation, async (req, res, next) => {
  try {
    const errors = await validationResult(req).formatWith(errorFormatter)
    if (!errors.isEmpty()) {
      return outputParser.fail(res, 400, errors.mapped(), null, 'Validation Error')
    }
    const { transaction_id } = await matchedData(req)

    await Package.deleteOne({ transaction_id })
      .then(() => {
        return outputParser.success(res, 200, true, 'Successfully delete data')
      })
      .catch(err => {
        throw new Error(err)
      })
  } catch (err) {
    next(err)
  }
})

module.exports = router