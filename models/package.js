const Mongoose = require('mongoose')

const origin_destination = Mongoose.Schema({
  customer_name: {
    type: String,
    trim: true,
    required: true,
    uppercase: true
  },
  customer_address: {
    type: String,
    trim: true,
    required: true,
    uppercase: true
  },
  customer_email: {
    type: String,
    trim: true
  },
  customer_phone: {
    type: String,
    trim: true,
    required: true
  },
  customer_address_detail: {
    type: String,
    trim: true
  },
  customer_zip_code: {
    type: String,
    trim: true,
    required: true
  },
  zone_code: {
    type: String,
    trim: true,
    required: true
  },
  organization_id: {
    type: Number,
    required: true
  },
  location_id: {
    type: String,
    trim: true,
    required: true,
    minlength: 24,
    maxlength: 24
  }
}, { _id: false })

const packageModel = Mongoose.Schema({
  transaction_id: {
    type: String,
    required: true,
    minlength: 36,
    maxlength: 36
  },
  customer_name: {
    type: String,
    uppercase: true,
    trim: true,
    required: true
  },
  customer_code: {
    type: Number,
    required: true
  },
  transaction_amount: {
    type: Number,
    required: true
  },
  transaction_discount: {
    type: Number,
    default: 0
  },
  transaction_additional_field: {
    type: String,
    trim: true
  },
  transaction_payment_type: {
    type: Number,
    required: true
  },
  transaction_state: {
    type: String,
    trim: true,
    required: true
  },
  transaction_code: {
    type: String,
    trim: true,
    required: true
  },
  transaction_order: {
    type: Number,
    required: true
  },
  location_id: {
    type: String,
    required: true,
    trim: true,
    minlength: 24,
    maxlength: 24
  },
  organization_id: {
    type: Number,
    required: true
  },
  created_at: {
    type: Date,
    default: Date.now
  },
  updated_at: {
    type: Date,
    default: Date.now
  },
  transaction_payment_type_name: {
    type: String,
    trim: true,
    required: true
  },
  transaction_cash_amount: {
    type: Number,
    default: 0
  },
  transaction_cash_change: {
    type: Number,
    default: 0
  },
  customer_attribute: {
    required: true,
    type: Mongoose.Schema({
      Nama_Sales: {
        type: String,
        trim: true,
        required: true,
        minlength: 3
      },
      TOP: {
        type: String,
        trim: true,
        required: true
      },
      Jenis_Pelanggan: {
        type: String,
        trim: true,
        required: true
      }
    }, { _id: false })
  },
  connote: {
    required: true,
    type: Mongoose.Schema({
      connote_id: {
        type: String,
        trim: true,
        required: true,
        minlength: 36,
        maxlength: 36
      },
      connote_number: {
        type: Number,
        required: true
      },
      connote_service: {
        type: String,
        trim: true,
        required: true
      },
      connote_service_price: {
        type: Number,
        required: true
      },
      connote_amount: {
        type: Number,
        required: true
      },
      connote_code: {
        type: String,
        trim: true,
        required: true,
        minlength: 17,
        maxlength: 17
      },
      connote_booking_code: {
        type: String,
        trim: true
      },
      connote_order: {
        type: Number,
        required: true
      },
      connote_state: {
        type: String,
        trim: true,
        required: true
      },
      connote_state_id: {
        type: Number,
        required: true
      },
      zone_code_from: {
        type: String,
        trim: true,
        required: true
      },
      zone_code_to: {
        type: String,
        trim: true,
        required: true
      },
      surcharge_amount: {
        type: Number
      },
      transaction_id: {
        type: String,
        trim: true,
        required: true,
        minlength: 36,
        maxlength: 36
      },
      actual_weight: {
        type: Number,
        required: true
      },
      volume_weight: {
        type: Number,
        default: 0
      },
      chargeable_weight: {
        type: Number,
        required: true
      },
      created_at: {
        type: Date,
        default: Date.now
      },
      updated_at: {
        type: Date,
        default: Date.now
      },
      organization_id: {
        type: Number,
        required: true
      },
      location_id: {
        type: String,
        trim: true,
        required: true,
        minlength: 24,
        maxlength: 24
      },
      connote_total_package: {
        type: Number,
        required: true
      },
      connote_surcharge_amount: {
        type: Number,
        default: 0
      },
      connote_sla_day: {
        type: Number,
        required: true
      },
      location_name: {
        type: String,
        trim: true,
        required: true
      },
      location_type: {
        type: String,
        trim: true,
        required: true
      },
      source_tariff_db: {
        type: String,
        trim: true,
        required: true
      },
      id_source_tariff: {
        type: Number,
        required: true
      },
      pod: {
        type: String,
        trim: true
      },
      history: []
    }, { _id: false })
  },
  connote_id: {
    type: String,
    trim: true,
    required: true,
    minlength: 36,
    maxlength: 36
  },
  origin_data: {
    required: true,
    type: origin_destination
  },
  destination_data: {
    required: true,
    type: origin_destination
  },
  koli_data: {
    required: true,
    type: [Mongoose.Schema({
      koli_length: {
        type: Number,
        default: 0
      },
      awb_url: {
        type: String,
        trim: true,
        required: true
      },
      created_at: {
        type: Date,
        default: Date.now
      },
      koli_chargeable_weight: {
        type: Number,
        required: true
      },
      koli_width: {
        type: Number,
        default: 0
      },
      koli_surcharge: [],
      koli_height: {
        type: Number,
        default: 0
      },
      updated_at: {
        type: Date,
        default: Date.now
      },
      koli_description: {
        type: String,
        trim: true,
        required: true
      },
      koli_formula_id: {
        type: String
      },
      connote_id: {
        type: String,
        trim: true,
        required: true,
        minlength: 36,
        maxlength: 36
      },
      koli_volume: {
        type: Number,
        default: 0
      },
      koli_weight: {
        type: Number,
        required: true
      },
      koli_id: {
        type: String,
        trim: true,
        required: true,
        minlength: 36,
        maxlength: 36
      },
      koli_custom_field: Mongoose.Schema({
        awb_sicepat: {
          type: String
        },
        harga_barang: {
          type: String
        }
      }, { _id: false }),
      koli_code: {
        type: String,
        trim: true,
        required: true,
        minlength: 19,
        maxlength: 19
      }
    }, { _id: false })]
  },
  custom_field: {
    required: true,
    type: Mongoose.Schema({
      catatan_tambahan: {
        type: String
      }
    }, { _id: false })
  },
  currentLocation: {
    required: true,
    type: Mongoose.Schema({
      name: {
        type: String,
        trim: true,
        required: true
      },
      code: {
        type: String,
        trim: true,
        required: true
      },
      type: {
        type: String,
        trim: true,
        required: true
      }
    }, { _id: false })
  }
})

module.exports = Mongoose.model('package', packageModel)