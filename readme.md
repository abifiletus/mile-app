# Mile App Test

[![Version](https://badge.fury.io/gh/tterb%2FHyde.svg)](https://badge.fury.io/gh/tterb%2FHyde)
[![Generic badge](https://img.shields.io/badge/nodejs-express-<COLOR>.svg)](https://shields.io/)


## Development

1. Create `.env` file in root folder. You can use `.env.example` as a template
2. Run `npm install` to download dependencies
3. Run `npm start` to start the application
4. Open application at `http://localhost:3000`

## Unit testing
Run `npm run test` to start testing.
Then you can see the result of testing on terminal.