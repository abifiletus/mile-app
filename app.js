const express = require('express')
const cookieParser = require('cookie-parser')
const logger = require('morgan')
const helmet = require('helmet')
const { fail } = require('./utils/outputParser')

require('dotenv/config')
require('./config/db')

const indexRouter = require('./routes/index')

const app = express()

app.use(logger('dev'))
app.use(helmet())
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(cookieParser())

app.use('/package', indexRouter)

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  return fail(res, 404, '', '', 'Request path not found')
})

// error handler
app.use(function(err, req, res, next) {
  console.log(err)
  return fail(res, (err.status || 500), err.message, err, 'Internal Server Error')
})

app.listen(process.env.PORT, () =>
  console.log(`App is listening on port ${process.env.PORT}.`)
)

module.exports = app